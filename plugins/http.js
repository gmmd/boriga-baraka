import axios from 'axios'

export default ({ app, store, redirect }, inject) => {
    let instance = axios.create({
        baseURL: process.env.API_URL
    })
    instance.interceptors.request.use((request) => {
        if (store.state.user.accessToken)
            request.headers.Authorization = 'Bearer ' + store.state.user.accessToken
        return request
    })
    instance.interceptors.response.use((response) => {
        return response
    }, error => {
        if (error.response && error.response.data && error.response.data.status == "forbidden_error") {
            store.dispatch('user/logout').then(() => {
                redirect('/register')
            })
        }
        return Promise.reject(error)
    })
    inject('http', instance)
}