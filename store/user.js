export const state = () => ({
    accessToken: localStorage.getItem('accessToken')
})

export const getters = {}

export const mutations = {
    setAccessToken(state, token) {
        localStorage.setItem('accessToken', token);
        state.accessToken = token;
    }
}
export const actions = {
    login({ commit }, token) {
        commit('setAccessToken', token)
    },
    logout({ commit }) {
        commit('setAccessToken', '')
    }
}
